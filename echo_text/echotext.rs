use std::io;

fn main() {
    println!("Hello, what's your name?");

    let mut name = String::new();

    io::stdin().read_line(&mut name).expect("Failed to read line");

    println!("Nice to meet you, {}!", name.trim());
}
