#![allow(dead_code)]
enum Direction {
    North,
    East,
    South,
    West,
}

fn main() {
    let direction = Direction::North;
    
    match direction {
        Direction::North => println!("Heading North!"),
        Direction::East => println!("Heading East!"),
        Direction::South => println!("Heading South!"),
        Direction::West => println!("Heading West!"),
    }
}
