use rss::Channel;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    // URL of the RSS feed
    let url = "https://feeds.bbci.co.uk/news/rss.xml";

    // Fetch the RSS feed
    let content = reqwest::blocking::get(url)?.bytes()?;
    let channel = Channel::read_from(&content[..])?;

    println!("Latest headlines from {}:", channel.title());
    
    // Display the first 5 headlines
    for (i, item) in channel.items().iter().take(5).enumerate() {
        println!("{}. {}", i + 1, item.title().unwrap_or("No title"));
    }

    Ok(())
}

