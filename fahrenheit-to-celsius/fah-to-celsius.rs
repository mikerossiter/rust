use std::io;

fn main() {
	println!("Input temperature in Fahrenheit: ");

	let mut fah_temp = String::new();

    io::stdin().read_line(&mut fah_temp).expect("Failed to read line");
    let f: u32 = fah_temp.trim().parse().expect("Please enter a number");

    let c = (f - 32) * 5/9;

    println!("{} degrees is {} celsius", f, c);

}