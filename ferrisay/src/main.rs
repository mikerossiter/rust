use ansi_term::{Colour, Style};
use std::io;

fn main() {
    let mut text = String::new();
    io::stdin()
        .read_line(&mut text)
        .expect("Failed to read line");

    let text = text.trim();
    let mut wrapped_text = String::new();
    let mut char_count = 0;

    for word in text.split_whitespace() {
        if char_count + word.len() + 1 > 66 {
            wrapped_text.push_str("\n");
            char_count = 0;
        }
        wrapped_text.push_str(word);
        wrapped_text.push(' ');
        char_count += word.len() + 1;
    }

    let bubble_length = wrapped_text.trim().len() + 4;
    let top_bottom_bubble = "*".repeat(bubble_length);
    let bottom_v = "V";
    let half_length = " ".repeat(bubble_length / 2);
    let speech_bubble = format!(
        "{}\n* {} *\n{}\n{}{}",
        top_bottom_bubble,
        wrapped_text.trim(),
        top_bottom_bubble,
        half_length,
        bottom_v
    );

    let ferris = format!(
        "
{}
             ^^^^^^^^^
     ()()  <           > ()()
      ||  <   0    0    > ||
       ===  __  o   __  ===
          VV  -----   VV      
",
        speech_bubble
    );
    let style = Style::default().fg(Colour::RGB(255, 165, 0));
    println!("{}", style.paint(ferris));
}
