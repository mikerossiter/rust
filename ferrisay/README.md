# FERRISAY 
 
## What does Ferris say? That's entirely up to you! 

This is inspired by `cowsay` but written in Rust and featuring an adorable ASCII Ferris instead. 
 
To build on Linux systems (*assuming you've installed Rust first*) run `cargo build` and place the binary from `/target/debug/` into `/use/local/bin/` or similar to run. 
 
Then feed it a string by running `echo "I ❤️ Rust " | ferrisay` and watch in amazement as Ferris speaks to you from the terminal. 
 
Get creative by inputting `fortune` or your favourite quotes. 