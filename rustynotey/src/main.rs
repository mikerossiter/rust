use dirs;
use std::fs::OpenOptions;
use std::io::{self, BufRead, BufReader, Write};

fn main() {
    let file_path = match dirs::home_dir() {
        Some(mut path) => {
            path.push("SavedNotes");
            path
        }
        None => {
            println!("Unable to determine home directory.");
            return;
        }
    };

    let file = match OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .append(true)
        .open(&file_path)
    {
        Ok(file) => file,
        Err(err) => {
            println!("Error opening file: {}", err);
            return;
        }
    };

    let mut notes: Vec<String> = BufReader::new(&file)
        .lines()
        .map(|line| line.unwrap())
        .collect();

    println!("Welcome to RustyNotey!");

    loop {
        println!("What would you like to do?");
        println!("1. View saved noteys");
        println!("2. Add a notey");
        println!("3. Delete a notey");
        println!("4. Exit");

        let mut input = String::new();
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read input");

        match input.trim() {
            "1" => {
                println!("Your noteys:");
                for (i, note) in notes.iter().enumerate() {
                    println!("{}. {}", i + 1, note);
                }
            }
            "2" => {
                println!("Enter your notey:");
                let mut new_note = String::new();
                io::stdin()
                    .read_line(&mut new_note)
                    .expect("Failed to read input");
                notes.push(new_note.trim().to_string());
                println!("Notey added!");
            }
            "3" => {
                println!("Enter the number of the notey you want to deletey:");
                let mut delete_index = String::new();
                io::stdin()
                    .read_line(&mut delete_index)
                    .expect("Failed to read input");

                // Convert input to integer
                let delete_index: usize = match delete_index.trim().parse() {
                    Ok(num) => num,
                    Err(_) => {
                        println!("Invalid input, please enter a number");
                        continue; // Restart the loop
                    }
                };

                // Check if the index is valid
                if delete_index > 0 && delete_index <= notes.len() {
                    // Remove the note at the specified index
                    notes.remove(delete_index - 1);
                    println!("Note deleted!");
                } else {
                    println!("Invalid index, please enter a valid number");
                }
            }
            "4" => break,
            _ => println!("Invalid input, please try again"),
        }
    }

    // Clear the file
    file.set_len(0).unwrap();

    // Write notes back to the file
    for note in &notes {
        writeln!(&file, "{}", note).unwrap();
    }

    println!("Exiting RustyNotey. Your noteys are saved. Byeee!");
}
