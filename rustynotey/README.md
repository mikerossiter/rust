# RustyNotey

🗒️ RustyNotey is a simple note or notey-taking app written in Rust that runs in the Linux terminal. 🗒️

## Features

- View existing noteys
- Add new noteys
- Delete old noteys
- Notes persist across sessions

## Getting Started

To use RustyNotey, simple clone this repo and run:

```bash
cargo run
```

or run:

```bash
cargo build --release
```

and copy the binary to /usr/local/bin