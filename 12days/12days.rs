fn main() {
	let days = ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh", "twelth"];
	let first = "a partridge in a pear tree.";
	let twelth = "twelve drummers drummin' ";
	let eleventh = "eleven pipers pipin', ";
	let tenth = "ten lords a leapin', ";
	let ninth = "nine ladies dancin', ";
	let eighth = "eight maids milkin', ";
	let seventh = "seven swans a swimmin', ";
	let sixth = "six geese a layin', ";
	let fifth = "five gold rings, ";
	let fourth = "four calling birds, ";
	let third = "three french hens, ";
	let second = "two turtle doves, ";
	let end = "and a partridge in a pear tree!";

	println!("On the {} day of Christmas my true love sent to me {}", days[0], first);
	println!("On the {} day of Christmas my true love sent to me {}{}", days[1], second, end);
	println!("On the {} day of Christmas my true love sent to me {}{}{}", days[2], third, second, end);
	println!("On the {} day of Christmas my true love sent to me {}{}{}{}", days[3], fourth, third, second, end);
	println!("On the {} day of Christmas my true love sent to me {}{}{}{}{}", days[4], fifth, fourth, third, second, end);
	println!("On the {} day of Christmas my true love sent to me {}{}{}{}{}{}", days[5], sixth, fifth, fourth, third, second, end);
	println!("On the {} day of Christmas my true love sent to me {}{}{}{}{}{}{}", days[6], seventh, sixth, fifth, fourth, third, second, end);
	println!("On the {} day of Christmas my true love sent to me {}{}{}{}{}{}{}{}", days[7], eighth, seventh, sixth, fifth, fourth, third, second, end);
	println!("On the {} day of Christmas my true love sent to me {}{}{}{}{}{}{}{}{}", days[8], ninth, eighth, seventh, sixth, fifth, fourth, third, second, end);
	println!("On the {} day of Christmas my true love sent to me {}{}{}{}{}{}{}{}{}{}", days[9], tenth, ninth, eighth, seventh, sixth, fifth, fourth, third, second, end);
	println!("On the {} day of Christmas my true love sent to me {}{}{}{}{}{}{}{}{}{}{}", days[10], eleventh, tenth, ninth, eighth, seventh, sixth, fifth, fourth, third, second, end);
	println!("On the {} day of Christmas my true love sent to me {}{}{}{}{}{}{}{}{}{}{}{}", days[11], twelth, eleventh, tenth, ninth, eighth, seventh, sixth, fifth, fourth, third, second, end);
}