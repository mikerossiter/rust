enum DayOfTheWeek {
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday,
}

static TODAY: DayOfTheWeek = DayOfTheWeek::Monday;

fn main() {
    match TODAY {
        DayOfTheWeek::Monday => println!("Today is Monday"),
        DayOfTheWeek::Tuesday => println!("Today is Tuesday"),
        DayOfTheWeek::Wednesday => println!("Today is Wednesday"),
        DayOfTheWeek::Thursday => println!("Today is Thursday"),
        DayOfTheWeek::Friday => println!("Today is Friday"),
        DayOfTheWeek::Saturday | DayOfTheWeek::Sunday => println!("It's the weekend!"),
    }
}
