use::rand::prelude::*;

fn main() {
    let number = thread_rng().gen_range(1..10);
    println!("Your 10 sided die lands on a {}.", number);
}
