use std::io;
use rand::Rng;

fn main() {
    println!("Welcome to the Higher/Lower Number Guessing Game!");
    
    // Generate a random number between 1 and 100
    let secret_number = rand::thread_rng().gen_range(1..=100);

    // Uncomment the line below during development to see the secret number
    // println!("Secret number: {}", secret_number);
    let mut guess_count = 0;
    let max_attempts = 10;

    // Main game loop
    loop {
        println!("Please enter your guess:");

        // Read the player's guess from the console
        let mut guess = String::new();
        io::stdin().read_line(&mut guess)
            .expect("Failed to read line");

        // Convert the guess to a number
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Please enter a valid number!");
                continue;
            }
        };

        guess_count += 1;

        // Check the guess against the secret number
        match guess.cmp(&secret_number) {
            std::cmp::Ordering::Less => println!("{} is too low! Try again.", guess),
            std::cmp::Ordering::Greater => println!("{} is too high! Try again.", guess),
            std::cmp::Ordering::Equal => {
                println!("Congratulations! You guessed the correct number in {} guesses!", guess_count);
                break; // Exit the game loop
            }
        }

        if guess_count >= max_attempts {
            println!("Sorry! You've reached the limit of ten guesses! The correct number was {}.", secret_number);
            break;
        }

    }
}
