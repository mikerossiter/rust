use std::io;


fn fibonacci(n: u32) -> u64 {
    if n == 0 {
        return 0;
    } else if n == 1 {
        return 1;
    } else {
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}

fn main() {
    
    println!("Input degree of index of Fibonacci sequence i.e. 1, 2, 3, 4, etc. ");

    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Failed to read line");
    let n: u32 = input.trim().parse().expect("Please enter a number");

    let result = fibonacci(n);

    println!("Index {} of the Fibonacci sequence equals {}", n, result);

}
