use rand::seq::SliceRandom;
use std::io;

#[derive(Debug, Clone, Copy, PartialEq)]
enum Color {
    Red,
    Green,
    Blue,
    Purple,
    White,
    Black,
    Orange,
    Yellow,
}

impl Color {
    fn to_ansi_escape(&self) -> &str {
        match self {
            Color::Red => "\x1b[31m",    // Red
            Color::Green => "\x1b[32m",  // Green
            Color::Blue => "\x1b[34m",   // Blue
            Color::Purple => "\x1b[35m", // Purple
            Color::White => "\x1b[37m",  // White
            Color::Black => "\x1b[30m",  // Black
            Color::Orange => "\x1b[33m", // Orange
            Color::Yellow => "\x1b[93m",
        }
    }
}

fn generate_secret_combination() -> [Color; 4] {
    let mut rng = rand::thread_rng();
    let colors = [
        Color::Red,
        Color::Green,
        Color::Blue,
        Color::Purple,
        Color::White,
        Color::Black,
        Color::Orange,
        Color::Yellow,
    ];

    let mut combination = [Color::Red; 4];
    for i in 0..4 {
        combination[i] = *colors.choose(&mut rng).unwrap();
    }

    combination
}

fn get_user_guess() -> [Color; 4] {
    println!("Enter your guess (Choose from: Red, Green, Blue, Orange, Black, White, Purple, Yellow; e.g., R G B O):");

    let mut guess = String::new();
    io::stdin().read_line(&mut guess).expect("Failed to read line");

    let colors: Vec<Color> = guess
        .trim()
        .split_whitespace()
        .map(|s| match s.to_uppercase().as_str() {
            "R" => Color::Red,
            "G" => Color::Green,
            "B" => Color::Blue,
            "P" => Color::Purple,
            "W" => Color::White,
            "K" => Color::Black,
            "O" => Color::Orange,
            "Y" => Color::Yellow,
            _ => {
                println!("Invalid color: {}", s);
                Color::Red
            }
        })
        .collect();

    if colors.len() != 4 {
        println!("Invalid number of colors. Please enter four colors.");
        return get_user_guess();
    }

    let mut guess_array = [Color::Red; 4];
    guess_array.copy_from_slice(&colors);

    guess_array
}

fn provide_feedback(secret: &[Color; 4], guess: &[Color; 4]) {
    println!("Feedback:");

    for (i, &color) in guess.iter().enumerate() {
        if color == secret[i] {
            print!("{}●\x1b[31m ", color.to_ansi_escape()); // Red dot for correct color and correct place
        } else if secret.contains(&color) {
            print!("{}●\x1b[93m ", color.to_ansi_escape()); // Yellow dot for correct color but in the wrong place
        } else {
            print!("- ");
        }
    }

    println!();
}

fn display_colored_dots(colors: &[Color; 4]) {
    print!("Your guess: ");
    for &color in colors.iter() {
        print!("{}●\x1b[0m ", color.to_ansi_escape());
    }
    println!();
}

fn main() {
    println!("Welcome to the Color Guessing Game!");

    let secret_combination = generate_secret_combination();

    for attempt in 1..=10 {
        let user_guess = get_user_guess();

        display_colored_dots(&user_guess);

        if user_guess == secret_combination {
            println!("Congratulations! You guessed the correct combination.");
            break;
        } else {
            println!("Incorrect guess. Try again! (Attempt {} of 10)", attempt);
            provide_feedback(&secret_combination, &user_guess);

            if user_guess.iter().all(|&color| secret_combination.contains(&color)) {
                println!("Well done! You guessed all colors correctly.");
                break;
            }
        }

        if attempt == 10 {
            println!("Game over. You've reached the maximum number of attempts.");
            println!("The correct combination was: {:?}", secret_combination);
        }
    }
}
